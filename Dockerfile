FROM alpine:latest

RUN echo '#!/usr/bin/env ash' > /usr/local/bin/entrypoint.sh && \
    echo 'crond -fd8 &' >> /usr/local/bin/entrypoint.sh && \
    echo 'rsync -avz --delete rsync://getcryst.al/repo /repo' >> /usr/local/bin/entrypoint.sh && \
    echo 'date >> /repo/timestamp' >> /usr/local/bin/entrypoint.sh && \
    echo 'caddy file-server --root /repo --listen :8000 --browse' >> /usr/local/bin/entrypoint.sh && \
    chmod +x /usr/local/bin/entrypoint.sh

RUN echo '#!/usr/bin/env ash' > /usr/local/bin/sync.sh && \
    echo 'rsync -avz --delete rsync://getcryst.al/repo /repo' >> /usr/local/bin/sync.sh && \
    echo 'date >> /repo/timestamp' >> /usr/local/bin/sync.sh && \
    chmod +x /usr/local/bin/sync.sh

RUN apk add --no-cache rsync caddy && \
    echo "*/30 * * * * /usr/local/bin/sync.sh" >> /etc/crontabs/root

CMD ["/usr/local/bin/entrypoint.sh"]
